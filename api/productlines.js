var connection = require('../connection');

function ProductLine() 
{
  this.get = function(res) 
  {
    connection.acquire(function(err, con) {
      con.query('select * from productlines', function(err, result) {
        con.release();
        res.send(result);
      });
    });
  };

  this.findOne = function(id, res) 
  {
    connection.acquire(function(err, con) {
      con.query('select * from productlines where productLine = ?', [id], function(err, result) {
        con.release();
        res.send(result);
      });
    });
  };

  this.create = function(productline, res) {
    connection.acquire(function(err, con) {
      con.query('insert into productlines set ?', productline, function(err, result) {
        con.release();
        if (err) {
          res.send({status: 1, message: 'ProductLine creation failed'});
        } else {
          res.send({status: 0, message: 'ProductLine created successfully'});
        }
      });
    });
  };

  this.update = function(productline, res) {
    connection.acquire(function(err, con) {
      con.query('update productlines set ? where productline = ?', [productline, productlines.productLine], function(err, result) {
        con.release();
        if (err) {
          res.send({status: 1, message: 'ProductLine update failed'});
        } else {
          res.send({status: 0, message: 'ProductLine updated successfully'});
        }
      });
    });
  };

  this.delete = function(id, res) {
    connection.acquire(function(err, con) {
      con.query('delete from productlines where productLine = ?', [id], function(err, result) {
        con.release();
        if (err) {
          res.send({status: 1, message: 'Failed to delete'});
        } else {
          res.send({status: 0, message: 'Deleted successfully'});
        }
      });
    });
  };

}

module.exports = new ProductLine();