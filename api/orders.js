var connection = require('../connection');

function Order() 
{
  this.get = function(res) 
  {
    connection.acquire(function(err, con) {
      con.query('select * from orders', function(err, result) {
        con.release();
        res.send(result);
      });
    });
  };

  this.findOne = function(id, res) 
  {
    connection.acquire(function(err, con) {
      con.query('select * from orders where orderNumber = ?', [id], function(err, result) {
        con.release();
        res.send(result);
      });
    });
  };

  this.create = function(order, res) {
    connection.acquire(function(err, con) {
      con.query('insert into orders set ?', order, function(err, result) {
        con.release();
        if (err) {
          res.send({status: 1, message: 'Order creation failed'});
        } else {
          res.send({status: 0, message: 'Order created successfully'});
        }
      });
    });
  };

  this.update = function(order, res) {
    connection.acquire(function(err, con) {
      con.query('update orders set ? where orders = ?', [order, order.orderNumber], function(err, result) {
        con.release();
        if (err) {
          res.send({status: 1, message: 'Order update failed'});
        } else {
          res.send({status: 0, message: 'Order updated successfully'});
        }
      });
    });
  };

  this.delete = function(id, res) {
    connection.acquire(function(err, con) {
      con.query('delete from orders where orderNumber = ?', [id], function(err, result) {
        con.release();
        if (err) {
          res.send({status: 1, message: 'Failed to delete'});
        } else {
          res.send({status: 0, message: 'Deleted successfully'});
        }
      });
    });
  };

}

module.exports = new Order();