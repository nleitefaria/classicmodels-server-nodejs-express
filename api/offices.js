var connection = require('../connection');

function Office() 
{
  this.get = function(res) 
  {
    connection.acquire(function(err, con) {
      con.query('select * from offices', function(err, result) {
        con.release();
        res.send(result);
      });
    });
  };

  this.findOne = function(id, res) 
  {
    connection.acquire(function(err, con) {
      con.query('select * from offices where officeCode = ?', [id], function(err, result) {
        con.release();
        res.send(result);
      });
    });
  };

  this.create = function(office, res) {
    connection.acquire(function(err, con) {
      con.query('insert into offices set ?', office, function(err, result) {
        con.release();
        if (err) {
          res.send({status: 1, message: 'Office creation failed'});
        } else {
          res.send({status: 0, message: 'Office created successfully'});
        }
      });
    });
  };

  this.update = function(office, res) {
    connection.acquire(function(err, con) {
      con.query('update offices set ? where officeCode = ?', [office, office.officeCode], function(err, result) {
        con.release();
        if (err) {
          res.send({status: 1, message: 'Office update failed'});
        } else {
          res.send({status: 0, message: 'Office updated successfully'});
        }
      });
    });
  };

  this.delete = function(id, res) {
    connection.acquire(function(err, con) {
      con.query('delete from offices where officeCode = ?', [id], function(err, result) {
        con.release();
        if (err) {
          res.send({status: 1, message: 'Failed to delete'});
        } else {
          res.send({status: 0, message: 'Deleted successfully'});
        }
      });
    });
  };

}

module.exports = new Office();