var connection = require('../connection');

function Product() 
{
  this.get = function(res) 
  {
    connection.acquire(function(err, con) {
      con.query('select * from products', function(err, result) {
        con.release();
        res.send(result);
      });
    });
  };

  this.findOne = function(id, res) 
  {
    connection.acquire(function(err, con) {
      con.query('select * from products where productCode = ?', [id], function(err, result) {
        con.release();
        res.send(result);
      });
    });
  };

  this.create = function(product, res) {
    connection.acquire(function(err, con) {
      con.query('insert into products set ?', product, function(err, result) {
        con.release();
        if (err) {
          res.send({status: 1, message: 'Product creation failed'});
        } else {
          res.send({status: 0, message: 'Product created successfully'});
        }
      });
    });
  };

  this.update = function(product, res) {
    connection.acquire(function(err, con) {
      con.query('update products set ? where productCode = ?', [product, product.productLine], function(err, result) {
        con.release();
        if (err) {
          res.send({status: 1, message: 'Product update failed'});
        } else {
          res.send({status: 0, message: 'Product updated successfully'});
        }
      });
    });
  };

  this.delete = function(id, res) {
    connection.acquire(function(err, con) {
      con.query('delete from products where productCode = ?', [id], function(err, result) {
        con.release();
        if (err) {
          res.send({status: 1, message: 'Failed to delete'});
        } else {
          res.send({status: 0, message: 'Deleted successfully'});
        }
      });
    });
  };

}

module.exports = new Product();