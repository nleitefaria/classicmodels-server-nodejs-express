var connection = require('../connection');

function OrderDetail() 
{
  this.get = function(res) 
  {
    connection.acquire(function(err, con) {
      con.query('select * from orderdetails', function(err, result) {
        con.release();
        res.send(result);
      });
    });
  };

  this.findOne = function(id, res) 
  {
    connection.acquire(function(err, con) {
      con.query('select * from orderdetails where orderNumber = ?', [id], function(err, result) {
        con.release();
        res.send(result);
      });
    });
  };

  this.create = function(orderdetail, res) {
    connection.acquire(function(err, con) {
      con.query('insert into orderdetails set ?', orderdetail, function(err, result) {
        con.release();
        if (err) {
          res.send({status: 1, message: 'OrderDetail creation failed'});
        } else {
          res.send({status: 0, message: 'OrderDetail created successfully'});
        }
      });
    });
  };

  this.update = function(orderdetail, res) {
    connection.acquire(function(err, con) {
      con.query('update orderdetails set ? where orderdetails = ?', [orderdetail, orderdetail.orderNumber], function(err, result) {
        con.release();
        if (err) {
          res.send({status: 1, message: 'OrderDetail update failed'});
        } else {
          res.send({status: 0, message: 'OrderDetail updated successfully'});
        }
      });
    });
  };

  this.delete = function(id, res) {
    connection.acquire(function(err, con) {
      con.query('delete from orderdetails where orderNumber = ?', [id], function(err, result) {
        con.release();
        if (err) {
          res.send({status: 1, message: 'Failed to delete'});
        } else {
          res.send({status: 0, message: 'Deleted successfully'});
        }
      });
    });
  };

}

module.exports = new OrderDetail();