var connection = require('../connection');

function Customer() 
{
  this.get = function(res) 
  {
    connection.acquire(function(err, con) {
      con.query('select * from customers', function(err, result) {
        con.release();
        res.send(result);
      });
    });
  };
  
  this.findOne = function(id, res) 
  {
    connection.acquire(function(err, con) {
      con.query('select * from customers where customerNumber = ?', [id], function(err, result) {
        con.release();
        res.send(result);
      });
    });
  };

  this.create = function(customer, res) {
    connection.acquire(function(err, con) {
      con.query('insert into customers set ?', customer, function(err, result) {
        con.release();
        if (err) {
          res.send({status: 1, message: 'Cusotmer creation failed'});
        } else {
          res.send({status: 0, message: 'Customer created successfully'});
        }
      });
    });
  };

  this.update = function(customer, res) {
    connection.acquire(function(err, con) {
      con.query('update customers set ? where customerNumber = ?', [customer, customer.customerNumber], function(err, result) {
        con.release();
        if (err) {
          res.send({status: 1, message: 'Cusotmer update failed'});
        } else {
          res.send({status: 0, message: 'Cusotmer updated successfully'});
        }
      });
    });
  };

  this.delete = function(id, res) {
    connection.acquire(function(err, con) {
      con.query('delete from customers where customerNumber = ?', [id], function(err, result) {
        con.release();
        if (err) {
          res.send({status: 1, message: 'Failed to delete'});
        } else {
          res.send({status: 0, message: 'Deleted successfully'});
        }
      });
    });
  };
}

module.exports = new Customer();