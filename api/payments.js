var connection = require('../connection');

function Payment() 
{
  this.get = function(res) 
  {
    connection.acquire(function(err, con) {
      con.query('select * from payments', function(err, result) {
        con.release();
        res.send(result);
      });
    });
  };

  this.findOne = function(id, res) 
  {
    connection.acquire(function(err, con) {
      con.query('select * from payments where customerNumber = ?', [id], function(err, result) {
        con.release();
        res.send(result);
      });
    });
  };

  this.create = function(payment, res) {
    connection.acquire(function(err, con) {
      con.query('insert into payments set ?', payment, function(err, result) {
        con.release();
        if (err) {
          res.send({status: 1, message: 'Payments creation failed'});
        } else {
          res.send({status: 0, message: 'Payments created successfully'});
        }
      });
    });
  };

  this.update = function(payment, res) {
    connection.acquire(function(err, con) {
      con.query('update payments set ? where payments = ?', [payment, payment.customerNumber], function(err, result) {
        con.release();
        if (err) {
          res.send({status: 1, message: 'Payment update failed'});
        } else {
          res.send({status: 0, message: 'Payment updated successfully'});
        }
      });
    });
  };

  this.delete = function(id, res) {
    connection.acquire(function(err, con) {
      con.query('delete from payments where customerNumber = ?', [id], function(err, result) {
        con.release();
        if (err) {
          res.send({status: 1, message: 'Failed to delete'});
        } else {
          res.send({status: 0, message: 'Deleted successfully'});
        }
      });
    });
  };

}

module.exports = new Payment();