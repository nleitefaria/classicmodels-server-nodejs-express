var customers = require('./api/customers');
var employees = require('./api/employees');
var offices = require('./api/offices');
var orderdetails = require('./api/orderdetails');
var orders = require('./api/orders');
var payments = require('./api/payments');
var productlines = require('./api/productlines');
var products = require('./api/products');

module.exports = 
{
  configure: function(app)
  {
  
	  ////////////////////////////////////////////////////////////////////////
	  //ROUTES FOR customers
	  ////////////////////////////////////////////////////////////////////////
  
    app.get('/customers/', function(req, res) {
      customers.get(res);
    });

	  app.get('/customers/:id', function(req, res) {
      customers.findOne(req.params.id, res);
    });

    app.post('/customers/', function(req, res) {
      customers.create(req.body, res);
    });

    app.put('/customers/', function(req, res) {
      customers.update(req.body, res);
    });

    app.delete('/customers/:id/', function(req, res) {
      customers.delete(req.params.id, res);
    });
	
	  ////////////////////////////////////////////////////////////////////////
	  //ROUTES FOR employees
  	////////////////////////////////////////////////////////////////////////

    app.get('/employees/', function(req, res) {
      employees.get(res);
    });

    app.get('/employees/:id', function(req, res) {
      employees.findOne(req.params.id, res);
    });

	
    app.post('/employees/', function(req, res) {
      employees.create(req.body, res);
    });

    app.put('/employees/', function(req, res) {
      employees.update(req.body, res);
    });

    app.delete('/employees/:id/', function(req, res) {
      employees.delete(req.params.id, res);
    });

    ////////////////////////////////////////////////////////////////////////
	  //ROUTES FOR offices
  	////////////////////////////////////////////////////////////////////////

    app.get('/offices/', function(req, res) {
      offices.get(res);
    });

    app.get('/offices/:id', function(req, res) {
      offices.findOne(req.params.id, res);
    });

	
    app.post('/offices/', function(req, res) {
      offices.create(req.body, res);
    });

    app.put('/offices/', function(req, res) {
      offices.update(req.body, res);
    });

    app.delete('/offices/:id/', function(req, res) {
      offices.delete(req.params.id, res);
    });

    ////////////////////////////////////////////////////////////////////////
	  //ROUTES FOR orderdetails
  	////////////////////////////////////////////////////////////////////////

    app.get('/orderdetails/', function(req, res) {
      orderdetails.get(res);
    });

    app.get('/orderdetails/:id', function(req, res) {
      orderdetails.findOne(req.params.id, res);
    });
	
    app.post('/orderdetails/', function(req, res) {
      orderdetails.create(req.body, res);
    });

    app.put('/orderdetails/', function(req, res) {
      orderdetails.update(req.body, res);
    });

    app.delete('/orderdetails/:id/', function(req, res) {
      orderdetails.delete(req.params.id, res);
    });


    ////////////////////////////////////////////////////////////////////////
	  //ROUTES FOR orders
  	////////////////////////////////////////////////////////////////////////

    app.get('/orders/', function(req, res) {
      orders.get(res);
    });

    app.get('/orders/:id', function(req, res) {
      orders.findOne(req.params.id, res);
    });

    app.post('/orders/', function(req, res) {
      orders.create(req.body, res);
    });

    app.put('/orders/', function(req, res) {
      orders.update(req.body, res);
    });

    app.delete('/orders/:id/', function(req, res) {
      orders.delete(req.params.id, res);
    });

    ////////////////////////////////////////////////////////////////////////
	  //ROUTES FOR payments
  	////////////////////////////////////////////////////////////////////////

    app.get('/payments/', function(req, res) {
      payments.get(res);
    });

    app.get('/payments/:id', function(req, res) {
      payments.findOne(req.params.id, res);
    });

    app.post('/payments/', function(req, res) {
      payments.create(req.body, res);
    });

    app.put('/payments/', function(req, res) {
      payments.update(req.body, res);
    });

    app.delete('/payments/:id/', function(req, res) {
      payments.delete(req.params.id, res);
    });

    ////////////////////////////////////////////////////////////////////////
	  //ROUTES FOR productlines
  	////////////////////////////////////////////////////////////////////////

    app.get('/productlines/', function(req, res) {
      productlines.get(res);
    });

    app.get('/productlines/:id', function(req, res) {
      productlines.findOne(req.params.id, res);
    });

    app.post('/productlines/', function(req, res) {
      productlines.create(req.body, res);
    });

    app.put('/productlines/', function(req, res) {
      productlines.update(req.body, res);
    });

    app.delete('/productlines/:id/', function(req, res) {
      productlines.delete(req.params.id, res);
    });

    ////////////////////////////////////////////////////////////////////////
	  //ROUTES FOR products
  	////////////////////////////////////////////////////////////////////////

    app.get('/products/', function(req, res) {
      products.get(res);
    });

    app.get('/products/:id', function(req, res) {
      products.findOne(req.params.id, res);
    });

    app.post('/products/', function(req, res) {
      products.create(req.body, res);
    });

    app.put('/products/', function(req, res) {
      products.update(req.body, res);
    });

    app.delete('/products/:id/', function(req, res) {
      products.delete(req.params.id, res);
    });
  }
  
};
